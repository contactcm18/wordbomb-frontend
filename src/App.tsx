import { useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Home from "pages/Home";
import Room from "pages/Room";
import RoomError from "pages/RoomError";
import UsernamePage from "pages/Username";
import LightMode, { DARKMODE_KEY } from "components/LightMode";

import "./App.scss";

function App() {
  const [darkMode, setDarkMode] = useState(
    window.localStorage.getItem(DARKMODE_KEY) === "true" || false
  );

  return (
    <main className={darkMode ? "dark" : ""}>
      <LightMode darkMode={darkMode} setDarkMode={setDarkMode} />
      <Router>
        <Switch>
          <Route path="/404">
            <RoomError />
          </Route>
          <Route path="/username">
            <UsernamePage />
          </Route>
          <Route path="/:roomId">
            <Room />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </Router>
    </main>
  );
}

export default App;
