import { useState } from "react";
import { Redirect, useLocation } from "react-router-dom";

import Username, { USERNAME_KEY } from "components/Username";

import styles from "./styles.module.scss";

function UsernamePage(props) {
  const { search } = useLocation();
  const [redirect, setRedirect] = useState("");

  const onConfirmUsername = () => {
    const username = window.localStorage.getItem(USERNAME_KEY);
    if (!username) return;

    const query = new URLSearchParams(search);
    const redirect_uri = query.get("redirect_path");
    if (!redirect_uri) return;

    setRedirect(redirect_uri);
  };

  return (
    <section className={styles.wrapper}>
      <h2>Set a username to play!</h2>
      <Username />
      <button onClick={onConfirmUsername}>Confirm</button>
      <small>You will be redirected back to the room.</small>
      {redirect && <Redirect to={redirect} />}
    </section>
  );
}

export default UsernamePage;
