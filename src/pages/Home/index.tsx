import { Helmet } from "react-helmet";

import Username from "components/Username";
import JoinRoom from "components/room/JoinRoom";
import CreateRoom from "components/room/CreateRoom";

import styles from "./styles.module.scss";

function Home() {
  return (
    <>
      <Helmet>
        <title>Wordbomb</title>
      </Helmet>
      <section className={styles.wrapper}>
        <h1 className={styles.header}>Wordbomb</h1>
        <Username />
        <JoinRoom />
        <p>Or</p>
        <CreateRoom />
      </section>
    </>
  );
}

export default Home;
