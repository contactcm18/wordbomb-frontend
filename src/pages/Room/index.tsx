import { useState, useEffect, useRef } from "react";
import { Redirect, useLocation } from "react-router-dom";
import { Helmet } from "react-helmet";
import { io } from "socket.io-client";

import Loader from "components/Loader";
import Chat from "components/room/Chat";
import UserDisplays from "components/room/UserDisplays";
import GameManager from "components/room/GameManager";
import Timer from "components/room/Timer";
import AnswerInput from "components/room/AnswerInput";
import ChallengeText from "components/room/ChallengeText";
import Countdown from "components/room/Countdown";

import { IGameState, IPlayer } from "../../lib/room/types";
import types from "lib/socket/types";

import styles from "./styles.module.scss";

function Room() {
  const { pathname } = useLocation();
  const [redirect, setRedirect] = useState("");

  // Game related
  const socket = useRef(
    io(process.env.REACT_APP_BACKEND_HOST || "", {
      withCredentials: true,
    })
  );
  const [gameState, setGameState] = useState<IGameState>();
  const [players, setPlayers] = useState<IPlayer[]>([]);

  useEffect(() => {
    //#region Events

    // If there is no username, redirect
    socket.current.on(types.ERROR_USERNAME, () =>
      setRedirect(`/username?redirect_path=${pathname}`)
    );
    // If the user is already in game, redirect
    socket.current.on(types.ERROR_IN_GAME, () => setRedirect("/"));
    // Update game state
    socket.current.on(types.ROOM_STATE_UPDATE, (gameState: IGameState) =>
      setGameState(gameState)
    );
    // Update player list
    socket.current.on(types.PLAYER_LIST_UPDATE, (newPlayerList) =>
      setPlayers(newPlayerList)
    );

    //#endregion

    if (socket.current.connected)
      socket.current.emit(types.ROOM_JOIN, pathname);
    else {
      socket.current.on("connect", () => {
        socket.current.emit(types.ROOM_JOIN, pathname);
      });
    }
  }, []);

  if (!gameState) return <Loader redirect={redirect} />;

  return (
    <div className={styles.wrapper}>
      <Helmet>
        <title>Wordbomb Game - {pathname}</title>
      </Helmet>
      <div className={styles.leftContent}>
        <div className={styles.upperLeftContent}>
          <UserDisplays players={players} gameState={gameState} />
          <ChallengeText
            challenge={gameState.currentChallenge}
            socket={socket.current}
          />
          <Timer
            active={gameState.started}
            nextTurnSpeed={gameState.settings.nextTurnSpeed}
            socket={socket.current}
          />
          <Countdown socket={socket.current} gameStarted={gameState.started} />
        </div>
        <AnswerInput
          socket={socket.current}
          currentTurn={gameState.currentTurn.userId}
        />
        <GameManager
          gameState={gameState}
          players={players}
          socket={socket.current}
        />
      </div>
      <Chat socket={socket.current} roomId={pathname} />
      {redirect && <Redirect to={redirect} />}
    </div>
  );
}

export default Room;
