import JoinRoom from "components/room/JoinRoom";

function RoomError() {
  return (
    <>
      <h1>This room does not exist.</h1>
      <p>Make sure you enter the right code!</p>
      <JoinRoom />
    </>
  );
}

export default RoomError;
