export interface IGameState {
  owner: string;
  started: boolean;
  currentTurn: {
    index: number;
    userId: string;
  };
  settings: {
    nextTurnSpeed: number;
    startingLives: number;
  };
  currentChallenge: string;
  turnsTakenOnChallenge: number;
  lives: {
    [playerId: string]: number;
  };
  scores: {
    [playerId: string]: number;
  };
}

export interface IPlayer {
  socketId: string;
  authId: string;
  username: string;
}
