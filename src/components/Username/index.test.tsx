import { render, fireEvent } from "@testing-library/react";

import Username, { USERNAME_KEY } from ".";

describe("set username by input", () => {
  const { getByLabelText } = render(<Username />);

  const INPUT_VALUE = "charlie12";
  const input = getByLabelText("username", { exact: false });

  it("should modify the input value", () => {
    fireEvent.change(input, { target: { value: INPUT_VALUE } });
    expect(input).toHaveValue(INPUT_VALUE);
  });

  it("should save username to local storage", () => {
    expect(window.localStorage.getItem(USERNAME_KEY)).toBe(INPUT_VALUE);
  });
});
