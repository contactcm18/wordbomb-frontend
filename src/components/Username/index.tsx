import { useState, useEffect, ChangeEvent, useRef } from "react";

export const USERNAME_KEY = "wb_username";

function Username() {
  const [username, setUsername] = useState("");
  const [errorMessages, setErrorMessages] = useState<string[]>([]);
  const usernameUpdate = useRef<NodeJS.Timeout | null>(null);

  // Check if user already has a set username
  useEffect(() => {
    const username = window.localStorage.getItem(USERNAME_KEY);
    if (username) setUsername(username);
  }, []);

  const onChangeUsername = (e: ChangeEvent<HTMLInputElement>) => {
    const newUsername = e.target.value;

    setUsername(newUsername);
    window.localStorage.setItem(USERNAME_KEY, newUsername);

    // Prevent from fetching after every quick keypress
    if (usernameUpdate.current) clearTimeout(usernameUpdate.current);
    usernameUpdate.current = setTimeout(async () => {
      try {
        const res = await fetch(
          `${process.env.REACT_APP_BACKEND_HOST}/auth/setUsername`,
          {
            method: "POST",
            credentials: "include",
            mode: "cors",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({ username: newUsername }),
          }
        );

        if (res.status !== 200) {
          const errorMessages = await res.json();
          setErrorMessages(errorMessages);
        }
      } catch (error) {
        setErrorMessages(["An error occurred."]);
      }
    }, 500);

    setErrorMessages([]);
  };

  return (
    <>
      <label htmlFor="username">Your username:</label>
      <input
        type="text"
        name="username"
        id="username"
        value={username}
        onChange={onChangeUsername}
      />
      {errorMessages.length > 0 && (
        <ul>
          {errorMessages.map((message) => (
            <li className="error">{message}</li>
          ))}
        </ul>
      )}
    </>
  );
}

export default Username;
