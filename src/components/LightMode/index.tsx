import { Dispatch, SetStateAction } from "react";
import { func, bool } from "prop-types";

import styles from "./styles.module.scss";

export const DARKMODE_KEY = "wb_darkmode";

interface ILightMode {
  darkMode: boolean;
  setDarkMode: Dispatch<SetStateAction<boolean>>;
}

function LightMode({ darkMode, setDarkMode }: ILightMode) {
  const onClick = () =>
    setDarkMode((dark) => {
      window.localStorage.setItem(DARKMODE_KEY, !dark ? "true" : "false");
      return !dark;
    });

  return (
    <button
      className={`${styles.button} ${darkMode ? styles.dark : ""}`}
      onClick={onClick}
      aria-hidden="true"
    ></button>
  );
}

LightMode.propTypes = {
  darkMode: bool.isRequired,
  setDarkMode: func.isRequired,
};

export default LightMode;
