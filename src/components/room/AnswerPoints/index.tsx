import { useState, useEffect } from "react";
import { object } from "prop-types";
import { Socket } from "socket.io-client";
import uniqid from "uniqid";

import TYPES from "lib/socket/types";

import styles from "./styles.module.scss";

interface IAnswerPoints {
  socket: Socket;
}

function AnswerPoints({ socket }: IAnswerPoints) {
  const [points, setPoints] = useState(0);
  const [updateState, setUpdateState] = useState("");

  useEffect(() => {
    socket.on(TYPES.GAME_GET_POINTS, (points: number) => {
      setUpdateState(uniqid());
      setPoints(points);
    });

    return () => {
      socket.off(TYPES.GAME_GET_POINTS);
    };
  }, []);

  return (
    <>
      {points > 0 && (
        <p className={styles.pointsText} key={updateState}>
          +{points}
        </p>
      )}
    </>
  );
}

AnswerPoints.propTypes = {
  socket: object.isRequired,
};

export default AnswerPoints;
