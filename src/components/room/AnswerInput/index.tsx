import { useEffect, useRef, FormEvent } from "react";
import { Socket } from "socket.io-client";
import { object } from "prop-types";

import types from "lib/socket/types";
import AnswerPoints from "../AnswerPoints";

import styles from "./styles.module.scss";

interface IAnswerInput {
  socket: Socket;
  currentTurn: string;
}

function AnswerInput({ socket, currentTurn }: IAnswerInput) {
  const form = useRef<HTMLFormElement>(null);
  const inputAnswer = useRef<HTMLInputElement>(null);

  const isMyTurn = socket.id === currentTurn;

  useEffect(() => {
    socket.on(types.GAME_INCORRECT, () => {
      // Trigger the error animation
      if (!form.current) return;
      form.current.classList.remove(styles.error);
      setTimeout(() => {
        if (!form.current) return;
        form.current.classList.add(styles.error);
      }, 100);
    });

    return () => {
      socket.off(types.GAME_INCORRECT);
    };
  }, []);

  useEffect(() => {
    if (inputAnswer.current) {
      if (isMyTurn) inputAnswer.current.focus();
      else {
        inputAnswer.current.blur();
        inputAnswer.current.value = "";
      }
    }
  }, [currentTurn]);

  const onSubmit = (e: FormEvent<HTMLFormElement>) => {
    if (!inputAnswer.current) return;
    socket.emit(types.GAME_ANSWER, inputAnswer.current.value);
    e.preventDefault();
  };

  const onInput = (e: FormEvent<HTMLInputElement>) => {
    if (!inputAnswer.current || !isMyTurn) return;
    socket.emit(types.GAME_USER_INPUT, inputAnswer.current.value);
  };

  const formClass = `${styles.form} ${isMyTurn ? styles.toggle : ""}`;

  return (
    <form onSubmit={onSubmit} className={formClass} ref={form}>
      <p className={`${styles.turnText} ${isMyTurn ? styles.show : ""}`}>
        It is your turn!
      </p>
      <input
        type="text"
        name="answer"
        id="answer"
        placeholder="Your answer..."
        onInput={onInput}
        ref={inputAnswer}
      />
      <button type="submit">Submit</button>
      <AnswerPoints socket={socket} />
    </form>
  );
}

AnswerInput.propTypes = {
  socket: object.isRequired,
};

export default AnswerInput;
