import { ChangeEvent } from "react";
import { Socket } from "socket.io-client";
import { bool, object } from "prop-types";

import { IGameState } from "lib/room/types";
import types from "lib/socket/types";

import styles from "./styles.module.scss";

interface IGameSettings {
  show: boolean;
  gameState: IGameState;
  socket: Socket;
}

function GameSettings({ show, gameState, socket }: IGameSettings) {
  const onChangeTurnSpeed = (e: ChangeEvent<HTMLInputElement>) =>
    socket.emit(types.GAME_CHANGE_TURN_SPEED, parseInt(e.target.value));

  const onChangeStartingLives = (e: ChangeEvent<HTMLInputElement>) =>
    socket.emit(types.GAME_CHANGE_STARTING_LIVES, parseInt(e.target.value));

  return (
    <section className={`${styles.wrapper} ${show ? styles.show : ""}`}>
      <div>
        <label htmlFor="turn-speed">
          <small>Turn speed (s):</small>
        </label>
        <input
          type="number"
          name="turn-speed"
          id="turn-speed"
          defaultValue={gameState.settings.nextTurnSpeed}
          min="2"
          max="30"
          onChange={onChangeTurnSpeed}
        />
      </div>
      <div>
        <label htmlFor="starting-lives">
          <small>Starting lives:</small>
        </label>
        <input
          type="number"
          name="starting-lives"
          id="starting-lives"
          defaultValue={gameState.settings.startingLives}
          min="1"
          max="8"
          onChange={onChangeStartingLives}
        />
      </div>
    </section>
  );
}

GameSettings.propTypes = {
  show: bool.isRequired,
  gameState: object.isRequired,
  socket: object.isRequired,
};

export default GameSettings;
