import { useState, useEffect, useRef } from "react";
import { object, bool } from "prop-types";
import { Socket } from "socket.io-client";

import types from "lib/socket/types";

import styles from "./styles.module.scss";

interface ICountdown {
  socket: Socket;
  gameStarted: boolean;
}

function Countdown({ socket, gameStarted }: ICountdown) {
  const [countdownTime, setCountdownTime] = useState(0);
  const countdownStatus = useRef<NodeJS.Timeout | null>(null);
  const oneSecond = 1000;

  useEffect(() => {
    socket.on(types.GAME_START_COUNTDOWN, (countdownTime: number) => {
      if (countdownStatus.current) clearTimeout(countdownStatus.current);
      countdownStatus.current = countdown(countdownTime);
    });

    return () => {
      socket.off(types.GAME_START_COUNTDOWN);
    };
  }, []);

  const countdown = (startTime?: number) => {
    if (startTime) setCountdownTime(startTime);
    else if (countdownTime <= 0) countdownStatus.current = null;

    return setTimeout(() => {
      countdownStatus.current = null;
      setCountdownTime((time) => time - oneSecond);
      countdownStatus.current = countdown();
    }, oneSecond);
  };

  return (
    <>
      {!gameStarted && countdownTime > 0 && (
        <h2 className={styles.countdownText}>
          Start in {countdownTime / oneSecond}s
        </h2>
      )}
    </>
  );
}

Countdown.propTypes = {
  socket: object.isRequired,
  gameStarted: bool.isRequired,
};

export default Countdown;
