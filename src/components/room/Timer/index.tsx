import { useState, useEffect } from "react";
import { number, bool } from "prop-types";
import { Socket } from "socket.io-client";
import uniqid from "uniqid";

import types from "lib/socket/types";

import styles from "./styles.module.scss";

interface ITimer {
  active: boolean;
  nextTurnSpeed: number;
  socket: Socket;
}

function Timer({ active, nextTurnSpeed, socket }: ITimer) {
  const [id, setId] = useState(0);

  useEffect(() => {
    socket.on(types.GAME_RESET_TIMER, () => setId(uniqid()));

    return () => {
      socket.off(types.GAME_RESET_TIMER);
    };
  }, []);

  return (
    <>
      {active && (
        <div
          key={id}
          className={styles.timer}
          style={{ "--timer-speed": `${nextTurnSpeed}s` } as any}
        ></div>
      )}
    </>
  );
}

Timer.propTypes = {
  active: bool.isRequired,
  nextTurnSpeed: number.isRequired,
};

export default Timer;
