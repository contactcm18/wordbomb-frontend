import { useEffect, useState } from "react";
import { Socket } from "socket.io-client";
import { string, object } from "prop-types";

import types from "lib/socket/types";

import styles from "./styles.module.scss";
import { isNoSubstitutionTemplateLiteral } from "typescript";

interface IChallengeText {
  challenge: string;
  socket: Socket;
}

function ChallengeText({ challenge, socket }: IChallengeText) {
  const [userInput, setUserInput] = useState("");

  useEffect(() => {
    socket.on(types.GAME_USER_INPUT, (value: string) => setUserInput(value));

    return () => {
      socket.off(types.GAME_USER_INPUT);
    };
  }, []);

  return (
    <>
      {challenge && (
        <div className={styles.challengeContainer}>
          <h1>{challenge}</h1>
          <p>{userInput ? userInput : <br></br>}</p>
        </div>
      )}
    </>
  );
}

ChallengeText.propTypes = {
  challenge: string.isRequired,
  socket: object.isRequired,
};

export default ChallengeText;
