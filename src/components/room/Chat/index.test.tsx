import { render, fireEvent, screen } from "@testing-library/react";
import { Socket, io } from "socket.io-client";
import uniqid from "uniqid";

import Chat from ".";
import { USERNAME_KEY } from "components/Username";
import types from "lib/socket/types";

// For this test to pass, there needs to be a connection to the server.
// Define server uri via process.env.REACT_APP_BACKEND_HOST

describe("chat functionality (must have connection to server)", () => {
  const ROOM_ID = uniqid();
  const USERNAME = "charlie12";

  let socket: Socket;
  beforeAll((done) => {
    // Create the socket instanace
    expect(process.env.REACT_APP_BACKEND_HOST).toBeTruthy();
    const s = io(process.env.REACT_APP_BACKEND_HOST || "", {
      withCredentials: true,
      // We don't have a token, inform the server we are a test
      auth: {
        token: process.env.SOCKET_AUTH,
      },
    });

    s.on("connect", () => {
      s.off("connect");
      // Join the room to receive messages
      s.emit(types.ROOM_JOIN, ROOM_ID);
      socket = s;
      done();
    });
  });

  afterAll(() => socket.disconnect());

  // Set our chat username
  window.localStorage.setItem(USERNAME_KEY, USERNAME);

  it("should append message to chat after pressing enter", async () =>
    await testSendChatMessage("this is a sentence", (textarea) =>
      fireEvent.keyDown(textarea, { key: "Enter", code: "Enter" })
    ));

  it("should append message to chat after pressing chat button", async () =>
    await testSendChatMessage("a test message 2", () => {
      const submitBtn = screen.getByText("Chat");
      fireEvent.click(submitBtn);
    }));

  const testSendChatMessage = async (
    message: string,
    submitEvent: (textarea: HTMLElement) => void
  ) => {
    const { getByPlaceholderText, findByText } = render(
      <Chat socket={socket} roomId={ROOM_ID} />
    );

    // Type a message
    const textarea = getByPlaceholderText("Send a message");
    fireEvent.change(textarea, { target: { value: message } });

    // Execute event to submit this message
    submitEvent(textarea);
    expect(textarea).toHaveValue("");

    // Appended message will appear at some point in near future
    const listItem = await findByText(new RegExp(`:.*${message}`, "s"));
    // Appended message must have username, which is a child "span" element
    const spanItem = listItem.firstChild;
    expect(spanItem).toHaveTextContent(USERNAME);
  };
});
