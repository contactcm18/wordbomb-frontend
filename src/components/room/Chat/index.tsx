import { useState, ChangeEvent, KeyboardEvent, useEffect } from "react";
import { Socket } from "socket.io-client";
import PropTypes from "prop-types";
import uniqid from "uniqid";

import { USERNAME_KEY } from "components/Username";
import types from "lib/socket/types";

import styles from "./styles.module.scss";

const MAX_MESSAGES = 40;

interface IChat {
  socket: Socket;
  roomId: string;
}

interface IMessage {
  username: string;
  text: string;
  id: string;
}

function Chat({ socket, roomId }: IChat) {
  const [messages, setMessages] = useState<IMessage[]>([]);
  const [message, setMessage] = useState("");

  useEffect(() => {
    socket.on(types.CHAT_RECEIVE, (message: IMessage) =>
      addMessageToChat(message)
    );
    return () => {
      socket.off(types.CHAT_RECEIVE);
    };
  }, []);

  const addMessageToChat = (message: IMessage) =>
    setMessages((messages) => {
      const newMessages = [...messages, message];
      if (newMessages.length > MAX_MESSAGES) newMessages.shift();
      return newMessages;
    });

  const onTypeMessage = (e: ChangeEvent<HTMLTextAreaElement>) =>
    setMessage(e.target.value);

  const onSubmitMessage = () => {
    const username = window.localStorage.getItem(USERNAME_KEY);
    if (!username || !message) return;

    const messageToSend: IMessage = { username, text: message, id: uniqid() };
    socket.emit(types.CHAT_SEND, roomId, messageToSend);
    setMessage("");
  };

  const onEnterTextarea = (e: KeyboardEvent<HTMLTextAreaElement>) => {
    if (e.key === "Enter") {
      onSubmitMessage();
      e.preventDefault();
    }
  };

  return (
    <section className={styles.wrapper}>
      <div className={styles.chatWrapper}>
        {messages.length > 0 && (
          <ul>
            {messages.map((message) => (
              <li key={message.id}>
                <span>{message.username}</span>: {message.text}
              </li>
            ))}
          </ul>
        )}
      </div>
      <textarea
        name="message"
        id="message"
        placeholder="Send a message"
        maxLength={500}
        style={{}}
        value={message}
        onChange={onTypeMessage}
        onKeyDown={onEnterTextarea}
      ></textarea>
      <button onClick={onSubmitMessage}>Chat</button>
    </section>
  );
}

Chat.propTypes = {
  socket: PropTypes.object.isRequired,
  roomId: PropTypes.string.isRequired,
};

export default Chat;
