import { render, fireEvent } from "@testing-library/react";
import { BrowserRouter as Router } from "react-router-dom";

import JoinRoom from ".";

it("should link to room code page", () => {
  const { getByLabelText, getByText } = render(
    <Router>
      <JoinRoom />
    </Router>
  );

  const ROOM_CODE = "azbdk";

  const input = getByLabelText("code", { exact: false });
  fireEvent.change(input, { target: { value: ROOM_CODE } });
  expect(input).toHaveValue(ROOM_CODE);

  const link = getByText("Join");
  expect(link).toHaveAttribute("href", `/${ROOM_CODE}`);
});
