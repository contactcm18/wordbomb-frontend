import { useState, ChangeEvent } from "react";
import { Link } from "react-router-dom";

function JoinRoom() {
  const [roomCode, setRoomCode] = useState("");

  const onChangeCode = (e: ChangeEvent<HTMLInputElement>) =>
    setRoomCode(e.target.value);

  return (
    <>
      <label htmlFor="code">Join by code:</label>
      <input
        type="text"
        name="code"
        id="code"
        value={roomCode}
        onChange={onChangeCode}
      />
      <Link
        to={roomCode.length ? `/${roomCode}` : "#"}
        className={`${roomCode.length ? "" : "disable"} button`}
      >
        Join
      </Link>
    </>
  );
}

export default JoinRoom;
