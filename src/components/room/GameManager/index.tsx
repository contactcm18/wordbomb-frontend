import { useState } from "react";
import { Socket } from "socket.io-client";
import { object, array } from "prop-types";

import { IGameState, IPlayer } from "lib/room/types";
import GameSettings from "components/room/GameSettings";
import types from "lib/socket/types";

import styles from "./styles.module.scss";

interface IGameManager {
  gameState: IGameState;
  players: IPlayer[];
  socket: Socket;
}

function GameManager({ gameState, players, socket }: IGameManager) {
  const [showSettings, setShowSettings] = useState(false);

  const isOwner = gameState.owner === socket.id;

  const startGame = () => socket.emit(types.GAME_START);

  const onClickSettings = () =>
    setShowSettings((previousValue) => !previousValue);

  return (
    <>
      {gameState.started === false && (
        <div className={styles.wrapper}>
          {players.length > 1 && isOwner ? (
            <>
              <small>You are the owner of this lobby...</small>
              <div className={styles.buttonsWrapper}>
                <button onClick={startGame}>Start Game</button>
                <button
                  className={styles.settings}
                  aria-label="open game settings"
                >
                  <img
                    src="/images/icons/settings.svg"
                    alt="settings"
                    onClick={onClickSettings}
                    width="28"
                    height="28"
                  />
                  <GameSettings
                    show={showSettings}
                    gameState={gameState}
                    socket={socket}
                  />
                </button>
              </div>
            </>
          ) : isOwner ? (
            <small>Waiting for players...</small>
          ) : (
            ""
          )}
          {!isOwner && <small>Waiting for game to start...</small>}
        </div>
      )}
    </>
  );
}

GameManager.propTypes = {
  gameState: object.isRequired,
  players: array.isRequired,
  socket: object.isRequired,
};

export default GameManager;
