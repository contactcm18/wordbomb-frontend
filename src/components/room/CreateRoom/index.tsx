import { Redirect } from "react-router-dom";
import { useState } from "react";
import uniqid from "uniqid";

function CreateRoom() {
  const [redirect, setRedirect] = useState("");

  const createGame = () => setRedirect(uniqid());

  return (
    <>
      <button onClick={createGame}>Create game</button>
      {redirect && <Redirect to={`/${redirect}`} />}
    </>
  );
}

export default CreateRoom;
