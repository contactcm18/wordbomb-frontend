import "@testing-library/react/dont-cleanup-after-each";
import { render, cleanup } from "@testing-library/react";

import { IGameState, IPlayer } from "lib/room/types";
import UserDisplays, { CSS_VAR_NUM_HEARTS } from ".";

const spreadSocketIdWithValues = (players: IPlayer[], value: number) =>
  players.reduce(
    (accumulator, player) => ({
      ...accumulator,
      [player.socketId]: value,
    }),
    {}
  );

describe("UserDisplays component", () => {
  afterAll(() => cleanup());

  const STARTING_HEARTS = 3;
  const SET_PLAYER_HEARTS_TO = 2; // 0 < value < STARTING_HEARTS

  const players: IPlayer[] = [
    { authId: "11a", socketId: "2a", username: "charl1" },
    { authId: "12a", socketId: "3a", username: "charl2" },
    { authId: "13a", socketId: "4a", username: "charl3" },
  ];

  const gameState: IGameState = {
    currentChallenge: "ab",
    currentTurn: {
      index: 0,
      userId: players[0].socketId,
    },
    lives: {
      ...spreadSocketIdWithValues(players, SET_PLAYER_HEARTS_TO),
    },
    owner: players[0].socketId,
    scores: {
      ...spreadSocketIdWithValues(players, 0),
    },
    settings: {
      nextTurnSpeed: 12,
      startingLives: STARTING_HEARTS,
    },
    started: true,
    turnsTakenOnChallenge: 0,
  };

  const { getAllByAltText, getByText, getByTestId } = render(
    <UserDisplays players={players} gameState={gameState} />
  );

  it("displays the correct # of full and empty hearts", () => {
    // Check that heart displays are correct
    const numEmptyHearts =
      (STARTING_HEARTS - SET_PLAYER_HEARTS_TO) * players.length;

    const numFullHearts = STARTING_HEARTS * players.length - numEmptyHearts;

    expect(getAllByAltText("full heart")).toHaveLength(numFullHearts);
    expect(getAllByAltText("empty heart")).toHaveLength(numEmptyHearts);
  });

  it("displays every username", () =>
    players.forEach((player) => getByText(player.username)));

  it("sets a timer speed CSS variable", () =>
    expect(getByTestId(CSS_VAR_NUM_HEARTS)).toHaveStyle(
      `${CSS_VAR_NUM_HEARTS}:${STARTING_HEARTS}`
    ));

  it("shows the correct users turn", () => {
    for (let i = 0; i < players.length; i++) {
      i === gameState.currentTurn.index
        ? expect(getByTestId(players[i].socketId)).toHaveClass("currentTurn")
        : expect(getByTestId(players[i].socketId)).not.toHaveClass(
            "currentTurn"
          );
    }
  });
});
