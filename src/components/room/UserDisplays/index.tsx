import { array, object } from "prop-types";

import { IGameState, IPlayer } from "lib/room/types";

import styles from "./styles.module.scss";

export const CSS_VAR_NUM_HEARTS = "--num-hearts";

interface IUserDisplays {
  players: IPlayer[];
  gameState: IGameState;
}

function UserDisplays({ players, gameState }: IUserDisplays) {
  /* Explanation of how it picks between a full or empty heart:

    starting      current       index
    3             3             0                  3+0 >= 3 // full
    3             3             1                  3+1 >= 3 // full
    3             3             2                  3+2 >= 3 // full
    
    3             2             0                  2+0 < 3 // empty heart
    3             2             1                  2+1 >= 3 // full heart
    3             2             2                  2+2 >= 3  // full heart

    3             1             0                  1+0 < 3 // empty
    3             1             1                  1+1 < 3 // empty
    3             1             2                  1+2 >= 3 // full

    3             0             0                  0+0 < 3 // empty
    3             0             1                  0+1 < 3 // empty
    3             0             2                  0+2 < 3 // empty
  */
  const getHeartSrc = (index: number, lives: number) =>
    lives + index >= gameState.settings.startingLives
      ? { src: "/images/icons/heart_full.svg", alt: "full heart" }
      : { src: "/images/icons/heart_empty.svg", alt: "empty heart" };

  const getLiClass = (socketId: string) =>
    `${styles.playerWrapper} ${
      socketId === gameState.currentTurn.userId ? styles.currentTurn : ""
    }`;

  return (
    <ul
      className={styles.wrapper}
      style={{ [CSS_VAR_NUM_HEARTS]: gameState.settings.startingLives } as any}
      data-testid={CSS_VAR_NUM_HEARTS}
    >
      {players.map((player) => (
        <li
          key={player.socketId}
          className={getLiClass(player.socketId)}
          data-points={gameState.scores[player.socketId]}
          data-testid={player.socketId}
        >
          <p>{player.username}</p>
          <div className={styles.heartsContainer}>
            {[...Array(gameState.settings.startingLives)].map(
              (value: undefined, i: number) => {
                const { src, alt } = getHeartSrc(
                  i,
                  gameState.lives[player.socketId]
                );
                return (
                  <img key={i} src={src} alt={alt} width="24" height="24" />
                );
              }
            )}
          </div>
        </li>
      ))}
    </ul>
  );
}

UserDisplays.propTypes = {
  players: array.isRequired,
  gameState: object.isRequired,
};

export default UserDisplays;
