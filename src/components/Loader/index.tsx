import { string } from "prop-types";
import { Redirect } from "react-router-dom";

import styles from "./styles.module.scss";

interface ILoader {
  redirect?: string;
}

function Loader({ redirect }: ILoader) {
  return (
    <section className={styles.wrapper}>
      <p>Loading...</p>
      {redirect && <Redirect to={redirect} />}
    </section>
  );
}

Loader.propTypes = {
  redirect: string,
};

export default Loader;
